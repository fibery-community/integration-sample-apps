const express = require(`express`);
const {logger, getRequestLogMiddlewares} = require(`./logger`);
const {schema} = require(`./schema`);
const {users} = require(`./users`);
const bodyParser = require(`body-parser`);
const path = require(`path`);

function createApp() {
    const app = express();

    app.use(getRequestLogMiddlewares());
    app.use(bodyParser.json());

    // get general app configuration
    app.get(`/`, (req, res) => {
        res.json({
            id: 'integration-sample-app',
            name: 'Integration Sample App',
            version: `1.0.0`,
            type: 'crunch',
            description: 'Integration sample app.',
            authentication: [
                {
                    description: 'Provide Token',
                    name: 'Token Authentication',
                    id: 'token',
                    fields: [
                        {
                            type: 'text',
                            description: 'Personal Token',
                            id: 'token',
                        },
                    ],
                },
            ],
            sources: [],
            responsibleFor: {
                dataSynchronization: true,
            },
        });
    });

    // get app logo
    app.get('/logo', (req, res) =>
        res.sendFile(path.resolve(__dirname, `./logo.svg`)),
    );

    // validate user account
    app.post(`/validate`, (req, res) => {
        const user = users[req.body.fields.token];
        if (user) {
            return res.json({
                name: user.name,
            });
        }

        res.status(401).json({message: `Unauthorized`});
    });

    // synchronization specific config that contains both types and filters
    app.post(`/api/v1/synchronizer/config`, (req, res) => {
        res.json({
            types: [
                {id: `flower`, name: `Flower`},
                {id: `regionPrice`, name: `Region Price`},
            ],
            filters: [],
        });
    });

    // get schema by types
    app.post(`/api/v1/synchronizer/schema`, (req, res) => {
        res.json(
            req.body.types.reduce((acc, type) => {
                acc[type] = schema[type];
                return acc;
            }, {}),
        );
    });

    // get data
    app.post(`/api/v1/synchronizer/data`, (req, res) => {
        const {requestedType, account} = req.body;

        return res.json({
            items: users[account.token].data[requestedType],
        });
    });

    app.use((req, res) => {
        res.status(404).json({message: `Not Found`});
    });

    // eslint-disable-next-line no-unused-vars
    app.use((err, req, res, next) => {
        logger.error(`error occurred`, err);
        res.status(500).json({message: err.message});
    });

    return app;
}

module.exports = {createApp};
