# Overview

Sample app based on express that shows how to build the simplest integration app for Fibery.

App includes:
* Token authentication
* Data fetching (without paging and delta synchronization)

# Usage

Run commands from project root:

* Install dependencies
    ```
    yarn install
    ```

* Run app
    ```
    yarn start-simple
  ``` 

**Make your custom integration app globally available**
 
It's possible to use ngrok or any other alternatives. Also it's possible to deploy this app on server.

How to use ngrok?

* Install [ngrok](https://www.npmjs.com/package/ngrok) to be able to make 
local http port available globally with valid https certificate
    ```
  npm i -g ngrok
  ```
  
* Run ngrok
    ```
  ngrok http 3911
  ```
  
* Copy https url and use it in Fibery when adding your custom app