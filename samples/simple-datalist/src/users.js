const users = {
    token1: {
        name: `Dad Sholler`,
        data: {
            flower: [
                {
                    id: `47fd45cf-5a07-40aa-9ee4-a4258832154a`,
                    name: `Rose`,
                },
                {
                    id: `4f3afc75-2fb9-4ff8-b26f-ab4bf2f470a3`,
                    name: `Lily`,
                },
                {
                    id: `d7525fc1-1979-4cfb-ba32-0dfab9280b24`,
                    name: `Tulip`,
                },
            ],
            regionPrice: [
                {
                    id: `56c50696-1d9f-4e4d-9678-448017d25474`,
                    flowerId: `d7525fc1-1979-4cfb-ba32-0dfab9280b24`,
                    price: 10,
                    name: `Eastern Europe`,
                },
                {
                    id: `503e5efb-7650-4c3a-85e9-f4ebc23adfd5`,
                    name: `Western Europe`,
                    flowerId: `d7525fc1-1979-4cfb-ba32-0dfab9280b24`,
                    price: 15,
                },
                {
                    id: `87ccd5ef-ed3f-49db-9bcc-c9d1daf91744`,
                    name: `Eastern Europe`,
                    price: 20,
                    flowerId: `47fd45cf-5a07-40aa-9ee4-a4258832154a`,
                },
            ],
        },
    },
    token2: {
        name: `Ben Dreamer`,
        data: {
            flower: [
                {
                    id: `4f3afc75-2fb9-4ff8-b26f-ab4bf2f470a3`,
                    name: `Lily`,
                },
                {
                    id: `d7525fc1-1979-4cfb-ba32-0dfab9280b24`,
                    name: `Tulip`,
                },
            ],
            regionPrice: [
                {
                    id: `c3352e8c-e62c-4e26-9a8b-852c1a3d2435`,
                    flowerId: `d7525fc1-1979-4cfb-ba32-0dfab9280b24`,
                    price: 10,
                    name: `East Coast`,
                },
                {
                    id: `7a581588-d5fc-46aa-a084-8e2b28f3d6e5`,
                    name: `West Coast`,
                    flowerId: `d7525fc1-1979-4cfb-ba32-0dfab9280b24`,
                    price: 15,
                },
                {
                    id: `6e510195-41e3-499b-9a76-9ad928720882`,
                    name: `Asia`,
                    price: 20,
                    flowerId: `4f3afc75-2fb9-4ff8-b26f-ab4bf2f470a3`,
                },
            ],
        },
    },
};

module.exports = {users};
