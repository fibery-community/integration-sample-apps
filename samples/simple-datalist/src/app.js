const express = require(`express`);
const {logger, getRequestLogMiddlewares} = require(`./logger`);
const {schema} = require(`./schema`);
const {users} = require(`./users`);
const bodyParser = require(`body-parser`);
const path = require(`path`);

function createApp() {
    const app = express();

    app.use(getRequestLogMiddlewares());
    app.use(bodyParser.json());

    // get general app configuration
    app.get(`/`, (req, res) => {
        res.json({
            id: 'integration-sample-datalist-app',
            name: 'Integration Sample Datalist App',
            version: `1.0.0`,
            type: 'crunch',
            description: 'Integration sample datalist app.',
            authentication: [
                {
                    description: 'Provide Token',
                    name: 'Token Authentication',
                    id: 'token',
                    fields: [
                        {
                            type: 'text',
                            description: 'Personal Token',
                            id: 'token',
                        },
                    ],
                },
            ],
            sources: [],
            responsibleFor: {
                dataSynchronization: true,
            },
        });
    });

    // get app logo
    app.get('/logo', (req, res) =>
        res.sendFile(path.resolve(__dirname, `./logo.svg`)),
    );

    // validate user account
    app.post(`/validate`, (req, res) => {
        const user = users[req.body.fields.token];
        if (user) {
            return res.json({
                name: user.name,
            });
        }

        res.status(401).json({message: `Unauthorized`});
    });

    // synchronization specific config that contains both types and filters
    app.post(`/api/v1/synchronizer/config`, (req, res) => {
        res.json({
            types: [
                {id: `flower`, name: `Flower`},
                {id: `regionPrice`, name: `Region Price`},
            ],
            filters: [
                {
                    id: `regions`,
                    title: 'Regions',
                    datalist: true,
                    optional: true,
                    type: 'multidropdown',
                },
            ],
        });
    });

    // get schema by types
    app.post(`/api/v1/synchronizer/schema`, (req, res) => {
        res.json(
            req.body.types.reduce((acc, type) => {
                acc[type] = schema[type];
                return acc;
            }, {}),
        );
    });

    // get datalist
    app.post(`/api/v1/synchronizer/datalist`, (req, res) => {
        res.json({
            items: [
                {
                    title: `Eastern Europe`,
                    value: `Eastern Europe`,
                },
                {
                    title: `Western Europe`,
                    value: `Western Europe`,
                },
                {
                    title: `East Coast`,
                    value: `East Coast`,
                },
                {
                    title: `West Coast`,
                    value: `West Coast`,
                },
                {
                    title: `Asia`,
                    value: `Asia`,
                },
            ],
        });
    });

    // get data
    app.post(`/api/v1/synchronizer/data`, (req, res) => {
        const {requestedType, account, filter} = req.body;

        if (requestedType === `flower`) {
            return res.json({
                items: users[account.token].data.flower,
            });
        }

        const regionPrices = users[account.token].data.regionPrice;
        const regions = filter.regions;
        if (!regions || regions.length === 0) {
            return res.json({
                items: regionPrices,
            });
        }

        return res.json({
            items: regionPrices.filter((price) => regions.includes(price.name)),
        });
    });

    app.use((req, res) => {
        res.status(404).json({message: `Not Found`});
    });

    // eslint-disable-next-line no-unused-vars
    app.use((err, req, res, next) => {
        logger.error(`error occurred`, err);
        res.status(500).json({message: err.message});
    });

    return app;
}

module.exports = {createApp};
