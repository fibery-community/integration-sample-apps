module.exports = {
    schema: {
        flower: {
            id: {
                name: `Id`,
                type: `id`,
            },
            name: {
                name: `Name`,
                type: `text`,
            },
        },
        regionPrice: {
            id: {
                name: `Id`,
                type: `id`,
            },
            name: {
                name: `Name`,
                type: `text`,
            },
            price: {
                name: `Price`,
                type: `number`,
            },
            flowerId: {
                name: `Flower Id`,
                type: `text`,
                relation: {
                    cardinality: `many-to-one`,
                    name: `Flower`,
                    targetName: `Region Prices`,
                    targetType: `flower`,
                    targetFieldId: `id`,
                },
            },
        },
    },
};
