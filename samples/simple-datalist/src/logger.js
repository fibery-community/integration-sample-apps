const {
    createLogger,
    createExpressRequestLogMiddleware,
} = require(`@fibery/vizydrop-logger`);

const logger = createLogger({
    level: `info`,
});

module.exports = {
    logger,
    getRequestLogMiddlewares: () => createExpressRequestLogMiddleware({logger}),
};
