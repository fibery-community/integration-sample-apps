const App = require(`./app`);
const {logger} = require(`./logger`);

const port = 3910;

const app = App.createApp();
app.listen(port, () => {
    logger.info(`app has been started on port: ${port}`);
});
