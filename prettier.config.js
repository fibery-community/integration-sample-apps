module.exports = {
    tabWidth: 4,
    singleQuote: true,
    trailingComma: `all`,
    overrides: [
        {
            files: ['*.json', '*.yml'],
            options: {
                tabWidth: 2,
            },
        },
    ],
    bracketSpacing: false,
};
